================
Challenges Ahead 
================
Cloud native technologies were not designed to run inside a vehicle. Therefore most of the available cloud native technology is missing several aspects, which are crucial for running inside the car. Multiple challenges are already identified for automotive cloud native.

Safety certification of cloud native technologies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A majority of the cloud native technologies, which are intended to bring the functionality inside the vehicle, are not safety certified. Often the used programming language itself is not suitable to ensure important features such as memory safety or determinism. Therefore, crucial components such as runtimes or orchestrators may need to be audited or rewritten.

Compatible safety certified system software
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Mixed critical orchestration is about management of applications with different safety requirements. Especially for functions with safety requirements the whole software stack underneath the orchestrator needs to be certified. To date, no automotive safety certified linux with container support is available. On the other hand, several certified OS systems don't support containers.

Startup
~~~~~~~
When starting a vehicle, multiple applications get started and requirements of availability are typically in the range of seconds. This implies that you need to have a prioritized order to start containers and the container runtime needs to start 10-100 containers in this time period. This relates to startup times lower than 10-50 ms per container (this presents a challenge for currently available runtimes). 

Real-time constraints & determinism
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
For automotive functions it is often mandatory to keep timing requirements during execution. Processing of signals chains (e.g., from sensors to applications control units and back to actuators) needs to be guaranteed to ensure important reaction times in order to avoid accidents. For this cloud native technology needs to be enhanced to support: 

* Deterministic scheduling 
* Deterministic processing of signals / guarantee for latency 
* Priority handling 

Automotive networks
~~~~~~~~~~~~~~~~~~~
The automotive network has time sensitive networking (TSN) enabled to achieve deterministic timing for data communication. Currently TSN is not supported in cloud native technologies. 
Resource constraints: The high-performance computers inside the vehicle are constrained environments. Containers are only allowed to consume a defined amount of resources (CPU, memory, IO, etc.). This should be well handled by available OS mechanisms but needs to be ensured during the development process.

Dynamic functions and homologation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A big benefit of an orchestrator is the capability of adding new applications during runtime. On the other hand automotive regulations require a homologation process of software before deployment. To unleash the full potential of orchestration in the vehicle a process to add or update applications dynamically needs to be established.

Workload Deployment
~~~~~~~~~~~~~~~~~~~
Automotive applications have different implications, restrictions and lifecycle when it comes to both releasing and deploying a new version of an end user application or a (operating) system level update. Existing DevOps methodologies can be applied to enhance this workflow with some refinement for the Automotive land.

Workload partitioning and clusters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To enable mixed safety systems often a hypervisor or microkernel is used to partition the hardware resources into domains running several Operating Systems in the car. The orchestrator has the ability to manage applications across separate partitions by adding agent nodes.

Cloud based Testing
~~~~~~~~~~~~~~~~~~~
Modern DevOps processes have robust testing infrastructure build into their testing and deployment pipelines. Currently this infrastructure from the cloud-native world lacks the ability to test and validate real-time workloads, or workloads that are targeting real-time or microcontroller based architectures. We need to enhance the cloud based tooling to meet the validation needs of safety and real-time domains. 

Addressing these and other challenges ahead will require industry cooperation and collaboration.
