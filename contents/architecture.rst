============
Architecture
============

Overview
--------
Considering the key factors and the motivation for SOAFEE, it becomes obvious that adopting concepts and solutions from cloud native in the vehicle will lead to a huge impact. The essential dimensions are: 
 * Functionality and interfaces for scaling and orchestrating services should work in the same way in the vehicle and in the cloud and follow cloud native technologies. This however needs to consider the Automotive-specific needs for safety and limited resource footprints. 
 * Development / Testing / Deployment workflows, which enable service development and testing in a cloud based virtual environment (simulation) before deploying to the physical hardware. We can move more test and validation scenarios to the cloud, if the execution environment in the virtual system is similar or equivalent to the one in the physical embedded system (environmental parity). 

.. image:: _static/images/soafee_arch_vision.*
   :width: 600
   :alt: SOAFEE Architecture Vision

Figure 1 - SOAFEE Architecture Vision

SOAFEE Software Architecture
----------------------------
At this stage in the SOAFEE journey to support software defined vehicles the SOAFEE software architecture can be represented by the elements in the following diagram.

.. image:: _static/images/soafee_arch_v1.0.*
   :width: 600
   :alt: SOAFEE Architecture v1.0

Figure 2 - SOAFEE Architecture v1.0 

   The required elements for this release of the SOAFEE architecture must include: 

   * OCI compliant container engine and runtime: `OCI Runtime Specification <https://github.com/opencontainers/runtime-spec>`_.
   * Kubernetes compatible container workload orchestration: `Kubernetes API <https://kubernetes.io/docs/concepts/overview/kubernetes-api/>`_.
   * Develop in Cloud, Deploy at the Edge (native cloud development) 
   * Reference Implementation Maintenance supported by CI: `meta-ewaol CI <https://gitlab.com/soafee/ewaol/meta-ewaol/-/pipelines>`_.
   * Runs unmodified on Platforms with Standard Based Firmware
  
The reference implementation of SOAFEE, `EWAOL <https://gitlab.com/soafee/ewaol/meta-ewaol>`_ is an expression of this release of the architecture.  The release numbers of EWAOL reflect the release of this document. 

Future Work 
-----------
The following topics need to be detailed in order to continue the adoption of cloud native technologies into the Automotive Domain: 

* Observability and Analysis: The paradigm of environmental parity, which underlines one of the central pillars of SOAFEE needs to be proven by dedicated observations and analytics. The major efficiency effect of a virtual development and validation in the cloud can only be leveraged, if we can prove that the results are comparable. 
* Mixed Critical Safety Orchestration: The paradigm shift for cloud native in Automotive needs to support the complete range of mixed criticality. Migration of applications towards Microservices requires significant investment. This is only acceptable, if the gains justify it. Harmonizing and simplifying the programming models in safety, non-safety, vehicle and cloud environments therefore is essential. The problem of mixed-criticality comprises considerations on appropriate tooling to allow flexible deployment of safety services during the entire lifecycle of the vehicle. 
* Cloud Native Trail: The adoption of Cloud Native in Automotive requires collaboration and contribution of many different stakeholders. It can only be driven by SOAFEE. To indicate a clear and stepwise approach for a migration, a clear trail needs to be described (similar to the `cloud native trail map <https://github.com/cncf/trailmap/blob/master/CNCF_TrailMap_latest.pdf>`_). 
* Partitioning Recipes: There are different options to achieve partitioning of compute (Containers, Hypervisor, upcoming technologies e.g. via eBPF). A clear comparison and guidelines are needed for a selection of the best possible solution for a dedicated use-case.
